# include <stdio.h>
# include <string.h>
# include <stdint.h>
# include <stdbool.h>
# include <sys/mman.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>
# include <getopt.h>
# include "stack.h"
# include "queue.h"
# include "huffman.h"
# include "code.h"
# include "bv.h"

int main(int argc, char ** argv)
{
    struct stat buffer;
    int fd = -1;
    bool verbose = false;
    FILE *output = NULL;
    int c;
    // Goes through all flags to see which flags are on
    while (( c = getopt ( argc , argv , " i:o:v" ) ) != -1)
    {
        switch (c)
        {
            case 'i':
            {
                fd = open( optarg, O_RDONLY );
                break;
            }
            case 'o':
            {
                output = fopen( optarg, "wb" );
                break;
            }
            case 'v':
            {
                verbose = true;
                break;
            }
            default:
            {
                printf("Not a valid flag!\n");
                break;
            }
        }
    }
    if (fd == -1)
    {
        printf("Invalid input file!\n");
        return 1;
    }
    if (output == NULL)
    {
        output = stdout;
    }
    
    // Creates memory map
    fstat(fd, &buffer);
    uint16_t fileSize = buffer.st_size;
    uint8_t *outFile;
    outFile = mmap(0, fileSize, PROT_READ, MAP_PRIVATE, fd, 0);
    if (outFile == MAP_FAILED)
    {
        printf("Mapping failed!\n");
        return 1;
    }
    
    // Keeps track of current reading position
    uint8_t *mmPoint = outFile;
    
    // Reads in magic number
    uint32_t magicNum;
    memcpy( &magicNum, mmPoint, sizeof(uint32_t) );
    mmPoint += 4;
    if (magicNum != 0xdeadd00d)
    {
        printf("Not a compressed file!\n");
        return 1;
    }
    
    // Reads in size of file
    uint64_t sizeOfFile;
    memcpy( &sizeOfFile, mmPoint, sizeof(uint64_t) );
    mmPoint += 8;
    
    // Reads in tree size
    uint16_t treeSize;
    memcpy( &treeSize, mmPoint, sizeof(uint16_t) );
    mmPoint += 2;
    
    if (verbose)
    {
        printf("Original %lu bits: tree (%u)\n", sizeOfFile * 8, treeSize);
    }
    
    // Loads tree
    treeNode *tree = loadTree( mmPoint, treeSize );
    mmPoint += treeSize;
    
    // Decodes compressed data
    uint8_t bit = 0;
    for (uint64_t i = 0; i < sizeOfFile; i++)
    {
        treeNode *s = getItem(tree, &mmPoint, &bit);
        //printf("%c",s->symbol);
        fputc( s->symbol, output );
    }
    
    delTree( tree );
    
    return 0;
}
