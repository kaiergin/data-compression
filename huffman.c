# include "huffman.h"

// Creates a new node for the tree
treeNode *newNode(uint8_t s, bool l, uint64_t c)
{
    treeNode *node = (treeNode *) calloc ( 1, sizeof(treeNode) );
    node->leaf = l;
    node->symbol = s;
    node->count = c;
    return node;
}

// Joins two nodes together and returns linking node
treeNode *join(treeNode *l, treeNode *r)
{
    treeNode *node = newNode( '$', false, l->count + r->count );
    node->right = r;
    node->left = l;
    return node;
}

// Deletes a tree recursively
void delTree(treeNode *t)
{
    if (t->leaf)
    {
        free(t);
    }
    else
    {
        delTree(t->right);
        delTree(t->left);
        free(t);
    }
    return;
}

// Steps through a tree following a uint32_t
// NOT USED
int32_t stepTree(treeNode *root, treeNode **t, uint32_t code)
{
    uint32_t value = 0x1;
    while (!root->leaf)
    {
        if (code & value)
        {
            root = root->left;
        }
        else
        {
            root = root->right;
        }
        value <<= 1;
    }
    *t = root;
    return 1; // Not sure what return is for
}

// Steps through a tree following a bitvector and a starting position
treeNode *getItem(treeNode *root, uint8_t **code, uint8_t *bit)
{
    uint8_t value = 0x1;
    value <<= *bit;
    while (!root->leaf) // Follow tree until leaf is found
    {
        if (*code[0] & value)
        {
            root = root->right; // If a 1, go right
        }
        else
        {
            root = root->left;
        }
        *bit += 1;
        value <<= 1;
        if (*bit == 8) // Once at end of byte
        {
            *bit = 0;
            value = 0x1;
            *code += 1;
        }
    }
    return root;
}

// Prints the tree
// Taken from Piazza
void printTree(treeNode *t, int depth)
{
        if (t && t->leaf)
        {
                if (isalnum(t->symbol))
                {
                        spaces(4 * depth); printf("'%c' (%lu)\n", t->symbol, t->count);
                }
                else
                {
                        spaces(4 * depth); printf("%X (%lu)\n", t->symbol, t->count);
                }
        }
        else if (t)
        {
                spaces(4 * depth); printf("$ (%lu)\n", t->count);
                printTree(t->left, depth + 1);
                printTree(t->right, depth + 1);
        }
        return;
}

// Creates code for each leaf in tree
void buildCode(treeNode *t, code s, code table[256])
{
    if (!t->leaf)
    {
        uint32_t scrap;
        pushCode( &s, 0 );
        buildCode( t->left, s, table );
        popCode( &s, &scrap );
        pushCode( &s, 1 );
        buildCode( t->right, s, table );
        popCode( &s, &scrap );
    }
    else
    {
        table[t->symbol] = s;
    }
}

// Dumps the tree to a file using reverse polish notation
void dumpTree(treeNode *t, FILE* file)
{
    if (!t->leaf)
    {
        dumpTree( t->left, file );
        dumpTree( t->right, file );
        fputc( 'I', file );
        //printf("I");
    }
    else
    {
        fputc( 'L', file );
        //printf("L%c", t->symbol);
        fputc( t->symbol, file );
    }
    return;
}

// Loads a tree from a file
treeNode *loadTree(uint8_t savedTree[], uint16_t treeBytes)
{
    stack *myStack = newStack();
    for (uint16_t i = 0; i < treeBytes; i++)
    {
        if (savedTree[i] == 'L')
        {
            i++;
            treeNode *tree = newNode( savedTree[i], true, 0 );
            push( myStack, tree );
        }
        else if (savedTree[i] == 'I')
        {
            treeNode *node1 = pop( myStack );
            treeNode *node2 = pop( myStack );
            treeNode *tree = join ( node2, node1 );
            push( myStack, tree );
        }
    }
    treeNode *node = pop( myStack );
    delStack( myStack );
    return node;
}
