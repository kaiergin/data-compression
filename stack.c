# include "stack.h"
# include <stdlib.h>

// Creates new stack
stack *newStack(void)
{
    stack *myStack = (stack *) calloc( 1, sizeof(stack) );
    myStack->size = 0;
    myStack->top = 6;
    myStack->entries = (twee **) malloc( 6 * sizeof( twee * ) );
    return myStack;
}

// Deletes stack
// ENTRIES MUST BE EMPTY
void delStack(stack *myStack)
{
    free(myStack->entries);
    free(myStack);
    return;
}

// Pops top off stack
twee * pop(stack *myStack)
{
    if ( !emptyS(myStack) )
    {
        twee *tree = myStack->entries[myStack->size - 1];
        myStack->size--;
        return tree;
    }
    return NIL;
}

// Pushes onto stack
void push(stack *myStack, twee *node)
{
    if ( fullS(myStack) )
    {
        myStack->top *= 2; // Reallocates 2x the size
        myStack->entries = (twee **) realloc( myStack->entries, (myStack->top) * sizeof( twee * ) );
    }
    myStack->entries[myStack->size] = node;
    myStack->size++;
    return;
}

bool emptyS(stack *myStack)
{
    if (myStack->size == 0)
    {
        return true;
    }
    return false;
}

bool fullS(stack *myStack)
{
    if (myStack->size == myStack->top)
    {
        return true;
    }
    return false;
}
