# ifndef _QUEUE_H
# define _QUEUE_H
# include <stdint.h>
# include <stdbool.h>
# include "huffman.h"

typedef treeNode * item; // treeNode defined in huffman.h

typedef struct queue
{
    uint32_t size;
    uint32_t head, tail;
    item *Q;
} queue;

queue *newQueue(uint32_t);
void delQueue(queue *);

bool empty(queue *);
bool full(queue *);

bool enqueue(queue *, item);
bool dequeue(queue *, item *);

bool oneQueue(queue *);

# endif
