# include <stdio.h>
# include <stdint.h>
# include <stdbool.h>
# include <getopt.h>
# include "stack.h"
# include "queue.h"
# include "huffman.h"
# include "code.h"
# include "bv.h"

int main(int argc, char ** argv)
{
    int c;
    FILE *file = NULL;
    FILE *writer = NULL;
    bool verbose = false;
    // Goes through all flags to see which flags are on
    while (( c = getopt ( argc , argv , " i:o:v" ) ) != -1)
    {
        switch (c)
        {
            case 'i':
            {
                file = fopen( optarg, "r" );
                break;
            }
            case 'o':
            {
                writer = fopen( optarg, "wb" );
                break;
            }
            case 'v':
            {
                verbose = true;
                break;
            }
            default:
            {
                printf("Not a valid flag!\n");
                break;
            }
        }
    }
    // Creates histogram
    if (file == NULL)
    {
        printf("File not found!\n");
        return 1;
    }
    uint32_t histogram[256];
    for (int i = 0; i < 256; i++)
    {
        histogram[i] = 0;
    }
    histogram[0] = 1;
    histogram[255] = 1;
    int buf = fgetc( file );
    uint64_t fileLen = 0;
    while (buf != EOF)
    {
        if (buf > 255)
        {
            printf("Error! Writing outside of buffer\n");
            return 1;
        }
        histogram[buf]++;
        fileLen++;
        buf = fgetc( file );
    }
    
    // Creates queue and adds items to queue
    uint16_t treeLen = 0;
    queue *q = newQueue( 256 );
    for (uint32_t i = 0; i < 256; i++)
    {
        if (histogram[i] != 0)
        {
            treeNode *node = newNode( (uint8_t)i, true, histogram[i] );
            bool f = enqueue( q, node );
            treeLen++;
            if (!f)
            {
                printf("Error adding to queue!\n");
                return 1;
            }
        }
    }
    
    // Creates tree out of nodes
    while ( !oneQueue(q) )
    {
        treeNode *node1;
        treeNode *node2;
        dequeue( q, &node1 );
        dequeue( q, &node2 );
        enqueue( q, join( node1, node2 ) );
    }
    // Takes last node off of queue
    treeNode *tree;
    dequeue( q, &tree );
    delQueue(q);
    
    // Creates code to navigate tree
    code s = newCode();
    code codeList[256];
    buildCode(tree, s, codeList);
    
    bitV *vec = newVec(fileLen*8);
    // COUNTER WILL BE OFF BY 1 WHEN SAVING
    // This is due to pointing to next free bit
    uint64_t counter = 0;
    rewind(file);
    while ((buf = fgetc(file)) != EOF)
    {
        code r = newCode(); // Buffer code
        uint32_t k;
        while (popCode(&codeList[buf], &k))
        {
            pushCode(&r, k); // pushes onto buffer
        }
        while (popCode(&r, &k))
        {
            pushCode(&codeList[buf], k); // pushes back onto original code
            if (k) // sets bits according to code
            {
                setBit(vec, counter++);
            }
            else
            {
                clrBit(vec, counter++);
            }
        }
    }
    
    fclose(file);
    
    // Outputs to terminal if no output file
    if (writer == NULL)
    {
        writer = stdout;
    }
    
    if (verbose)
    {
        printf("Original %lu bits: leaves %u (%u bytes)", fileLen * 8, treeLen, (treeLen * 3) - 1);
    }
    
    // Write magic number
    uint32_t magNum = 0xdeadd00d;
    fwrite( &magNum, 1, sizeof(uint32_t), writer );
    // Write size of original file
    fwrite( &fileLen, 1, sizeof(uint64_t), writer );
    // Write size of huffman tree
    treeLen = (treeLen * 3) - 1;
    fwrite( &treeLen, 1, sizeof(uint16_t), writer );
    
    // Write tree
    dumpTree( tree, writer );
    
    if (verbose)
    {
        printf(" encoding %lu bits (%lf%%).\n", counter, ((double)counter / ((double)fileLen * 8 )) * 100);
    }
    
    counter = (counter / 8) + 1;
    for (uint64_t i = 0; i < counter; i++ )
    {
        fputc( valByte(vec, i), writer );
    }
    
    fclose(writer);
    delVec( vec );
    delTree(tree);
    return 0;
}
