# include "queue.h"
# include <stdlib.h>

// Some functions guided by example 33 on Piazza

// Creates new queue
queue *newQueue(uint32_t size)
{
    queue *q = (queue *) calloc( 1, sizeof(queue) );
    size++;
    q->size = size;
    q->head = 0;
    q->tail = 0;
    q->Q = (item *)calloc( size, sizeof(item) );
    return q;
}

// Deletes queue
void delQueue(queue *q)
{
    free(q->Q);
    free(q);
    return;
}

// Checks if queue is empty
bool empty(queue *q)
{
    if (q->head == q->tail)
    {
        return true;
    }
    return false;
}

// Checks if queue is full
bool full(queue *q)
{
    if ((q->head + 1) % q->size == q->tail % q->size )
    {
        return true;
    }
    return false;
}

// Inserts into queue (in order from least to greatest)
bool enqueue(queue *q, item i)
{
    if ( !full(q) )
    {
        uint32_t temp = q->tail;
        while (temp % q->size != q->head) // Can make this more efficient
        {
            if (i->count < q->Q[temp % q->size]->count) // counts added here
            {
                item hold = q->Q[temp % q->size];
                q->Q[temp % q->size] = i;
                i = hold;
            }
            temp ++;
        }
        q->Q[q->head] = i;
        q->head = (q->head + 1) % q->size;
        return true;
    }
    return false;
}

// Pops off tail
bool dequeue(queue *q, item *i)
{
    if ( !empty(q) )
    {
        *i = q->Q[q->tail];
        q->tail = (q->tail + 1) % q->size;
        return true;
    }
    return false;
}

bool oneQueue(queue *q)
{
    if ((q->tail + 1) % q->size == q->head)
    {
        return true;
    }
    return false;
}
