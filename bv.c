#include <stdio.h>
#include <stdlib.h>
#include "bv.h"

// Creates new bitvector
bitV *newVec(uint32_t num)
{
    bitV *vec = (bitV *)calloc(1,sizeof(bitV));
    vec->v = (uint8_t *)calloc( (num >> 3) + 1, sizeof(uint8_t));
    vec->l = num;
    return vec;
}

// Returns length of bitvector
uint32_t lenVec(bitV *vec)
{
    return vec->l;
}

// Sets a bit to 1
void setBit(bitV *vec, uint32_t index)
{
    vec->v[index >> 3] |= 0x1 << index%8;
    return ;
}

// Returns value of a bit
uint8_t valBit(bitV *vec, uint32_t index)
{
    uint8_t temp = 0x1 << index%8;
    return vec->v[index >> 3] & temp;
}

// Sets a bit to 0
void clrBit(bitV *vec, uint32_t index)
{
    vec->v[index >> 3] &= ~(0x1 << index%8);
    return ;
}

// Returns the value of the byte
uint8_t valByte(bitV *vec, uint32_t count)
{
    return vec->v[count];
}

// Turns all bits on
void one(bitV *vec){
    for(uint32_t i = 0; i < vec->l >> 3; i++)
    {
        vec->v[i] = ~(0x0);
    }
    return ;
}

// Deletes a bitvector
void delVec(bitV *vec)
{
    free(vec->v);
    free(vec);
    return ;
}
