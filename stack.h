# ifndef _STACK_H
# define _STACK_H
# include <stdint.h>
# include <stdbool.h>
# include "huffman.h"

typedef struct DAH twee;

typedef struct stack
{
    uint32_t size;
    uint32_t top;
    twee **entries;
} stack;

stack *newStack(void);
void delStack(stack *);

twee * pop(stack *);
void push(stack *, twee *);

bool emptyS(stack *);
bool fullS(stack *);

# endif
