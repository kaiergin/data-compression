CC = gcc
CFLAGS = -Wall -Wextra -Wpedantic -Werror -std=c99 -g
ARGS = huffman.o queue.o stack.o bv.o

.PHONY: all
all:	encode decode 

encode:	$(ARGS) encode.o
	$(CC) $(CFLAGS) $(ARGS) encode.o -o encode
decode: $(ARGS) decode.o
	$(CC) $(CFLAGS) $(ARGS) decode.o -o decode
decode.o:	decode.c
	$(CC) $(CFLAGS) -c decode.c
huffman.o:	huffman.c
	$(CC) $(CFLAGS) -c huffman.c
encode.o:	encode.c
	$(CC) $(CFLAGS) -c encode.c
queue.o:	queue.c
	$(CC) $(CFLAGS) -c queue.c
stack.o:	stack.c
	$(CC) $(CFLAGS) -c stack.c
bv.o:	bv.c
	$(CC) $(CFLAGS) -c bv.c

.PHONY:	clean
clean:
	rm -f $(ARGS) encode.o decode.o encode decode
